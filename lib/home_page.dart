import 'package:dialyfood/monday.dart';
import 'package:dialyfood/myModel.dart';
import 'package:dialyfood/tuesday_page.dart';
import 'package:flutter/material.dart';

 class HomePage extends StatefulWidget {
   @override
   _HomePageState createState() => _HomePageState();
 }

 class _HomePageState extends State<HomePage> {
   int selectedIndex = 0;
   List<Widget> _pageList;

   MondayPage _mondayPage =  MondayPage();
   TuesdayPage _tuesdayPage =  TuesdayPage();

    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageList = [_mondayPage, _tuesdayPage];
  }
   @override
   Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         backgroundColor: Colors.white,
         title: Text("Daily Food",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
         shadowColor: Colors.pink[100],
         actions: [
           IconButton(icon: Icon(Icons.favorite_border),
               color: Colors.pink,
               onPressed: (){

               }),
           Container(
             margin: EdgeInsets.only(top: 3),
             padding: EdgeInsets.only(right: 10),
            width: 50,
            height: 50,
            child: Stack(
              children: [
                  IconButton(icon: Icon(Icons.add_shopping_cart),
                      color: Colors.grey,
                      onPressed: (){

                      }),
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 20,
                      height: 20,
                      decoration:
                        BoxDecoration(shape: BoxShape.circle,color: Colors.pink),
                        child: Center(
                            child: Text("10",style: TextStyle(color: Colors.white,fontSize: 15),))),
                  )

              ],
            ),
           )
         ],
       ),
       body: _buildBody(),
     );
   }
   PageController _pageController = PageController();
   _buildBody() {
     return Column(

       children: [
        SizedBox(
          height: 60.0,
          width: MediaQuery.of(context).size.width,
            child: Container(
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: mylist.length,
                  itemBuilder: (context,index) => _buildCategory(index)),

            ),

        ),

        Expanded(
          child: Container(
            child: PageView(
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              children: _pageList,
              onPageChanged: (index){
                setState(() {
                  selectedIndex = index;
                });
              },
            ),
          ),
        ),
      ],
     );


   }

  _buildCategory(index) {
     return GestureDetector(
         onTap: () {
           setState(() {
             selectedIndex = index;
             if (selectedIndex == index){
               _pageController.animateToPage(index, duration: Duration(milliseconds: 300), curve: Curves.easeOut);

             }

           });

         },
         child: Container(
           child: Padding(
             padding: const EdgeInsets.all(14.0),
             child: Column(
             // crossAxisAlignment: CrossAxisAlignment.center,
             children: [
               Text(
                 mylist[index].title,
                 style: TextStyle(
                   fontWeight: FontWeight.bold,
                   color: selectedIndex == index ? Colors.pink : Colors.grey
                 ),

               ),
               Container(
                 margin: EdgeInsets.all(1),
                 height: 2,
                 width: 30,
                 color: selectedIndex == index ? Colors.pink : Colors.transparent
               )
             ],
     ),
           ),
         ),
     );
  }

 }

