import 'package:flutter/material.dart';


class MondayPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  _buildBody() {
    return Container(
      padding: EdgeInsets.all(10),
      child: GridView.extent(
        maxCrossAxisExtent: 200,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        children: [
          Container(
            color: Colors.blue,
            child: Image.network("https://www.rasmeinews.com/wp-content/uploads/2018/09/SpicyChickenSoup-rasmeinews.jpg",
            fit: BoxFit.cover,),
          ),
          Container(
            color: Colors.blue,
            child: Image.network("https://camboglobal.files.wordpress.com/2019/06/ac8498a20ec0123774e80ba90206ff8abfa901ba-1.jpg",
              fit: BoxFit.cover,),
          ),
          Container(
            child: Image.network("https://4.bp.blogspot.com/-zlpZaKdPrds/WZWV7AwRRHI/AAAAAAAAIlE/qiHd_K14H8gMcj789DilbvCM30iUgpnhgCLcBGAs/s640/%25E1%259E%2586%25E1%259E%25B6%25E1%259E%2580%25E1%259F%2592%25E1%259E%258F%25E1%259F%2585%25E1%259E%259F%25E1%259E%25B6%25E1%259E%2585%25E1%259F%258B%25E1%259E%2598%25E1%259E%25B6%25E1%259E%2593%25E1%259F%258B%25E1%259F%25A4.jpg",
              fit: BoxFit.cover,),
          ),
          Container(
            child: Image.network("https://www.rasmeinews.com/wp-content/uploads/2018/09/SpicyChickenSoup-rasmeinews.jpg",
              fit: BoxFit.cover,),
          ),
        ],

      ),
    );
  }
}
